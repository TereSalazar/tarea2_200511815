var express = require("express");
var app = express();
var fs = require("fs");
var bodyParser = require("body-parser");
const request = require("request");
const axios = require("axios");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "500mb"
  })
);

app.get("/restaurante/:id", function(req, res) {
  fs.readFile(__dirname + "/" + "platillo.json", "utf8", function(err, data) {
    var platillos = JSON.parse(data);
    var platillo = platillos["platillo" + req.params.id];
    console.log(platillo);
    res.end(JSON.stringify(platillo));
    console.log("Enviando datos platillo");
  });
});

/*
 *   Recibe parametros idClilente idPlatillo direccion
 */
app.post("/orden", function(req, res) {
  var idC = req.body.idCliente;
  var idP = req.body.idPlatillo;
  var direc = req.body.direccion;

  axios.get("http://localhost:8000/cliente/" + idC).then(function(response) {
    console.log(response.data);

    var plat = datoPlatillo(idP);

    axios.get("http://localhost:8002/repartidor/1").then(function(response2) {
      datos = {
        nombreCliente: response.data.nombre,
        Platillo: plat,
        Direccion: direc,
        Repartidor: response2.data.nombre
      };
      res.send(datos);
      console.log(response2.data);
    });
  });
});

/*
 *   Funcion que retorna nombre del platillo
 */
function datoPlatillo(idP) {
  let arch = fs.readFileSync(__dirname + "/" + "platillo.json", "utf8");
  let data = JSON.parse(arch);

  let plato = data.find(valor => valor.idPlatillo == idP);

  console.log(plato.nombre);
  return plato.nombre;
}

var server = app.listen(8001, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Servidor escuchando", host, port);
});
