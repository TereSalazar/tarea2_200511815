# Servicio Cliente

Servicio encargado de enviar información del cliente, este funciona en el puerto localhost:8000/.

## Recursos

## cliente/id 

Este servicio utiliza un GET para devolver datos del cliente por lo que utilizamos un archivo json llamado cliente.json con la siguiente estructura:


[
~~~        
        idCliente: numero,
        nombre: valor,
        password: valor
~~~
]



