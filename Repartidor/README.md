# Servicio Repartidor

Servicio encargado de enviar información del repartidor, este funciona en el puerto localhost:8002/.

## Recursos

## repartidor/id

Este servicio utiliza un GET para devolver datos del repartidor por lo que utilizamos un archivo json llamado repartidor.json con la siguiente estructura:


[
~~~
        idCliente: numero,
        nombre: valor,
        password: valor
~~~
]