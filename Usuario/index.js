const inquirer = require("inquirer");
const request = require("request");

/*
 *   Arreglo de preguntas al usuario
 */
var preguntas = [
  {
    type: "input",
    name: "idCliente",
    message: "Ingrese Usuario"
  },
  {
    type: "input",
    name: "idPlatillo",
    message: "Ingrese Platillo"
  },
  {
    type: "input",
    name: "direccion",
    message: "Ingrese Direccion"
  }
];

menu();

/*
 *  Funcion para mostrar en consola el menu al usuario
 */
function menu() {
  inquirer.prompt(preguntas).then(answers => {
    crearOrden(
      answers["idCliente"],
      answers["idPlatillo"],
      answers["direccion"]
    );
    //console.log(`Hi ${answers['idCliente']}!`)
  });
}

/*
 *   Funcion que envia datos a POST de Restaurante
 */
function crearOrden(idC, idP, dir) {
  console.log("********************DETALLE ORDEN**********************");
  let obj = {
    idCliente: idC,
    idPlatillo: idP,
    direccion: dir
  };

  let options = {
    url: "http://localhost:8001/orden/",
    method: "POST",
    json: obj
  };

  request(options, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      console.log(body);
      console.log("Orden creada");
    } else {
      console.log("Error ");
    }
  });
}
