# TABLA DE CONTENIDO

* Información General
* Tecnología
* Instalación
* Video
* Test

## Información general

Simulación de servicios.

* Solicitud de comida por parte del cliente
* Recepcion de órdenes por parte del restaurante
* Servicio de entrega por repartidor

## Tecnología

* Node js version 12.14.1

## Instalación

* Descargar o clonar proyecto
* Dirigirse en consola a la carpeta donde se descargó o clonó el proyecto
* Escribir en consola node index.js para ejecutar el proyecto

## Video Demo

[enlace](https://youtu.be/0cdYuRQeDcY)
